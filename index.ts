import express, { urlencoded } from 'express';
import { Request,Response,NextFunction } from 'express';
import logger from 'morgan';
import fetch from 'node-fetch';
import redis from 'redis';

const PORT=process.env.PORT || 5000;
const REDIS_PORT= 7000;

const redisClient=redis.createClient(REDIS_PORT);

const app=express();
app.use(logger('dev'));

function setRepoonse(username : string,repos:number){
    return `<h2>${username} has ${repos} github repos </h2>`
}

async function getRepos(req:Request,res:Response,next :NextFunction){
    try{
        console.log("Fetching Data......");
        const username=req.params.username;
        const response=await fetch(`https://api.github.com/users/${username}`);
        const data=await response.json();
        const repos=data.public_repos;
        res.send({"public_repos":repos});

        redisClient.setex(username,3600,repos);
        res.send(setRepoonse(username,repos));

    }catch(e){
        console.log(e);
        res.status(500);
    }
}

app.get("/repos/:username",getRepos);

app.listen(PORT,()=>{
    console.log(`Started At Port : ${PORT}`);
})
